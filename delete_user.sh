#/bin/sh
OVPN_DATA=/root/docker/openvpn/openvpn-data
CLIENTNAME="$1"
docker exec  -it openvpn ovpn_revokeclient "$CLIENTNAME" 
docker exec  -it openvpn easyrsa gen-crl 
#clean ovpn
rm -rf "$CLIENTNAME.ovpn"
# restart
docker restart openvpn
