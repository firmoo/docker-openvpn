#/bin/sh
OVPN_DATA=/root/docker/openvpn/openvpn-data
CLIENTNAME="$1"
docker exec  -it openvpn easyrsa build-client-full "$CLIENTNAME" nopass
docker exec  -it openvpn ovpn_getclient "$CLIENTNAME" > "$CLIENTNAME.ovpn"
